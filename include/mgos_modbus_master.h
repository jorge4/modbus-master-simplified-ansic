/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/***************************************************
 * This is a library for the Adafruit STMPE610 Resistive
 * touch screen controller breakout
 * ----> http://www.adafruit.com/products/1571
 *
 * Check out the links above for our tutorials and wiring diagrams
 * These breakouts use SPI or I2C to communicate
 *
 * Adafruit invests time and resources providing this open source code,
 * please support Adafruit and open-source hardware by purchasing
 * products from Adafruit!
 *
 * Written by Limor Fried/Ladyada for Adafruit Industries.
 * MIT license, all text above must be included in any redistribution
 ****************************************************/

/* Adapted for native Mongoose by Pim van Pelt <pim@google.com> */

#ifndef __MODBUSMASTERH_
#define __MODBUSMASTERH_

#include "mgos.h"

double millis();

typedef void (*my_cb_type)(bool success, char *data, int len, void *userdata);

uint16_t compute_CRC16(uint8_t *puchMsg, uint16_t usDataLen);

/******************************************************************************
 * Defines and typedefs
 *****************************************************************************/
#define MODBUS_READH_FUNCTION 3
#define MODBUS_READI_FUNCTION 4
#define MODBUS_WRITEH_FUNCTION 6
#define MODBUS_WRITE_MULTIH_FUNCTION 16

#define MB_ERROR_NO_ERROR 0
#define MB_ERROR_CRC_MISMATCH -1
#define MB_ERROR_FUNC_MISMATCH -2
#define MB_ERROR_ADDRESS_MISMATCH -3
#define MB_ERROR_SLAVE_MISMATCH -4

#define MB_ERROR_CRC_MISMATCH_msg "crc mismatch"
#define MB_ERROR_FUNC_MISMATCH_msg "func mismatch"
#define MB_ERROR_ADDRESS_MISMATCH_msg "address mismatch"
#define MB_ERROR_SLAVE_MISMATCH_msg "slave id mismatch"

bool send_read_holding_request(uint8_t uartn, uint8_t mb_func, uint8_t slave_id, uint16_t address, uint16_t len);
bool send_write_holding_request(uint8_t uartn, uint8_t slave_id, uint16_t address, uint16_t data);
bool send_write_multiple_holding_request(uint8_t uartn, uint8_t slave_id, uint16_t address, uint16_t len, uint16_t *data);
// bool process_buffer_as_read_holding_resp();

#ifdef __cplusplus
extern "C"
{
#endif

    bool make_writeh_request(int uartn, int slave_id, int address, int data, my_cb_type cb, void *userdata);
    bool make_readh_request(int uartn, int slave_id, int address, int len, my_cb_type cb, void *userdata);
    bool make_readi_request(int uartn, int slave_id, int address, int len, my_cb_type cb, void *userdata);
    void setup_sentinels(int uartn, double timeout);
    uint8_t * get_response_buffer(int uartn);

#ifdef __cplusplus
}
#endif

bool mgos_modbus_master_simplified_ansic_init(void);

#define make_word(hi, lo) ((hi << 8) | lo)
double bin_to_float(uint8_t *data);
int bin_to_long(uint8_t *data);

int bin_to_long2(uint8_t *data);
double bin_to_float2(uint8_t *data);

#endif // __MODBUSMASTERH_
