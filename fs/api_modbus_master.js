let MODBUS_MASTER = {
    setup_bus_sentinels: ffi('void setup_sentinels(int, double)'),

    request_holding_registers: ffi('bool make_readh_request(int, int, int, int, void (*)(bool, char* , int, userdata), userdata)'),

    request_input_registers: ffi('bool make_readi_request(int, int, int, int, void (*)(bool, char* , int, userdata), userdata)'),

    // get_byte_from_response: ffi('char get_byte_from_response(int, int)'),
    _get_regval_from_readh_response: ffi('int get_regval_from_readh_response_with_offset(int, int, int)'),
    get_regval_from_readh_response: function (bus, idx, offset) { return this._get_regval_from_readh_response(bus, idx, offset === undefined ? 0 : offset) },
    _get_float_from_readh_response: ffi('double get_float_from_readh_response_with_offset(int, int, int)'),
    get_float_from_readh_response: function (bus, idx, offset) { return this._get_float_from_readh_response(bus, idx, offset === undefined ? 0 : offset) },

    _get_long_2_from_readh_response: ffi('int get_long_2_from_readh_response_with_offset(int, int, int)'),
    get_long_2_from_readh_response: function (bus, idx, offset) { return this._get_long_2_from_readh_response(bus, idx, offset === undefined ? 0 : offset) },
    _get_float_2_from_readh_response: ffi('int get_float_2_from_readh_response_with_offset(int, int, int)'),
    get_float_2_from_readh_response: function (bus, idx, offset) { return this._get_float_2_from_readh_response(bus, idx, offset === undefined ? 0 : offset) },

    write_holding_registers: ffi('bool make_writeh_request(int, int, int, int, void (*)(bool, char* , int, userdata), userdata)'),

}
