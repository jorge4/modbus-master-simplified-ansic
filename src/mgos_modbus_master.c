/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mgos.h"
#include "mgos_uart_hal.h"
#include "mgos_uart.h"
#include "mgos_debug.h"
#include "mgos_modbus_master.h"

//#include "esp32/include/esp_timer.h"
struct mgos_uart_state *mgos_uart_hal_get_state(int uart_no);

typedef struct
{
  uint8_t uartn;
  bool pending_transaction;
  double transaction_sent_ts;
  double transaction_last_rx_ts;
  double transaction_timeout;
  uint8_t input_buffer[256];
  uint8_t input_buffer_cnt;
  uint16_t last_transaction_bytes_expected;
  uint8_t last_transaction_func_expected;
  uint16_t last_transaction_address_expected;
  uint8_t last_transaction_slave_expected;
  my_cb_type completion_cb;
  void *userdata;
  //double alive_ts;
} modbus_state;

modbus_state mb_states[MGOS_MAX_NUM_UARTS];

#define push_byte(b) mybuffer[byte_cnt++] = b

inline uint16_t read_holding_request_expected_response_len(uint16_t len)
{
  return 1 /* slave id */ +
         1 /* func code */ +
         1 /* len */ +
         2 * len /* len ->uint16_t -> 2bytes */ +
         2 /* crc */;
}

inline uint16_t write_holding_request_expected_response_len()
{
  return 1 /* slave id */ +
         1 /* func code */ +
         2 /* address */ +
         2 /* data */ +
         2 /* crc */;
}

inline uint16_t write_multiple_holding_request_expected_response_len()
{
  return 1 /* slave id */ +
         1 /* func code */ +
         2 /* address */ +
         2 /* len */ +
         2 /* crc */;
}

uint16_t tx_buffer_pending(uint8_t uartn)
{
  // struct mgos_uart_state *ptr = ;
  return mgos_uart_hal_get_state(uartn)->tx_buf.len;
}

uint16_t bytes_to_expect_in_response(uint8_t function_code, uint16_t len)
{
  switch (function_code)
  {
  case MODBUS_READH_FUNCTION:
  case MODBUS_READI_FUNCTION:
    return read_holding_request_expected_response_len(len);
  case MODBUS_WRITEH_FUNCTION:
    return write_holding_request_expected_response_len();
  case MODBUS_WRITE_MULTIH_FUNCTION:
    return write_multiple_holding_request_expected_response_len();
  default:
    return -1;
  }
}

void set_pending_transaction(uint8_t uartn, uint8_t slave, uint8_t function_code, uint16_t address, uint16_t len, my_cb_type cb, void *userdata)
{
  modbus_state *state = &mb_states[uartn];
  state->pending_transaction = true;
  state->transaction_sent_ts = millis();
  state->transaction_last_rx_ts = millis();
  state->input_buffer_cnt = 0;
  state->last_transaction_bytes_expected = bytes_to_expect_in_response(function_code, len);
  state->last_transaction_func_expected = function_code;
  state->last_transaction_address_expected = address;
  state->last_transaction_slave_expected = slave;
  state->completion_cb = cb;
  state->userdata = userdata;
}

void clear_pending_transaction(uint8_t uartn)
{
  modbus_state *state = &mb_states[uartn];
  state->pending_transaction = false;
  memset(state->input_buffer, 0, state->input_buffer_cnt);
  state->input_buffer_cnt = 0;
  state->last_transaction_bytes_expected = -1;
  state->last_transaction_func_expected = -1;
  state->last_transaction_address_expected = -1;
  state->last_transaction_slave_expected = -1;
  state->completion_cb = NULL;
  state->userdata = NULL;
  // uint8_t dummy[10];
  // while (mgos_uart_read_avail(uartn))
  // {
  //   mgos_uart_read(uartn, dummy, 10);
  // }
}

bool send_read_holding_request(uint8_t uartn, uint8_t mb_func, uint8_t slave_id, uint16_t address, uint16_t len)
{
  uint8_t mybuffer[9];
  uint8_t byte_cnt = 0;
  mb_states[uartn].input_buffer_cnt = 0;
  push_byte(slave_id);
  push_byte(mb_func);
  push_byte(address >> 8);
  push_byte(address);
  push_byte(len >> 8);
  push_byte(len);
  uint16_t crc = compute_CRC16(mybuffer, byte_cnt);
  push_byte(crc);
  push_byte(crc >> 8);
  uint8_t *p = mybuffer;
  // LOG(LL_INFO, ("a enviar: %X %X %X %X %X %X %X %X ", mybuffer[0], mybuffer[1], mybuffer[2], mybuffer[3], mybuffer[4], mybuffer[5], mybuffer[6], mybuffer[7]));
  uint8_t wrote = mgos_uart_write(uartn, mybuffer, byte_cnt);
  //LOG(LL_INFO, ("wrote: %X %X %X %X %X %X %X %X -> %i (%i)", p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], wrote, tx_buffer_pending(uartn)));

  return wrote == byte_cnt;
}

bool send_write_holding_request(uint8_t uartn, uint8_t slave_id, uint16_t address, uint16_t data)
{
  uint8_t mybuffer[9];
  uint8_t byte_cnt = 0;
  mb_states[uartn].input_buffer_cnt = 0;
  push_byte(slave_id);
  push_byte(MODBUS_WRITEH_FUNCTION);
  push_byte(address >> 8);
  push_byte(address);
  push_byte(data >> 8);
  push_byte(data);
  uint16_t crc = compute_CRC16(mybuffer, byte_cnt);
  push_byte(crc);
  push_byte(crc >> 8);
  // LOG(LL_INFO, ("a enviar: %X %X %X %X %X %X %X %X ", outbuffer[0], outbuffer[1], outbuffer[2], outbuffer[3], outbuffer[4], outbuffer[5], outbuffer[6], outbuffer[7]));
  uint8_t wrote = mgos_uart_write(uartn, mybuffer, byte_cnt);
  return wrote == byte_cnt;
}

bool send_write_multiple_holding_request(uint8_t uartn, uint8_t slave_id, uint16_t address, uint16_t len, uint16_t *data)
{
  uint8_t mybuffer[9 + (len * 2)];
  uint8_t byte_cnt = 0;
  mb_states[uartn].input_buffer_cnt = 0;
  push_byte(slave_id);
  push_byte(MODBUS_WRITE_MULTIH_FUNCTION);
  push_byte(address >> 8);
  push_byte(address);
  push_byte(len >> 8);
  push_byte(len);
  push_byte(len * 2); // byte_len
  for (uint16_t i = 0; i < len; i++)
  {
    push_byte(*(data + i) >> 8);
    push_byte(*(data + i));
  }
  uint16_t crc = compute_CRC16(mybuffer, byte_cnt);
  push_byte(crc);
  push_byte(crc >> 8);
  // LOG(LL_INFO, ("a enviar: %X %X %X %X %X %X %X %X ", outbuffer[0], outbuffer[1], outbuffer[2], outbuffer[3], outbuffer[4], outbuffer[5], outbuffer[6], outbuffer[7]));
  uint8_t wrote = mgos_uart_write(uartn, mybuffer, byte_cnt);
  return wrote == byte_cnt;
}

bool make_writeh_request(int uartn, int slave_id, int address, int data, my_cb_type cb, void *userdata)
{
  //LOG(LL_INFO, ("readh! %d", slave_id));
  clear_pending_transaction(uartn);
  set_pending_transaction(uartn, slave_id, MODBUS_WRITEH_FUNCTION, address, 1, cb, userdata);
  if (!send_write_holding_request(uartn, slave_id, address, data))
  {
    clear_pending_transaction(uartn);
  }
  return true;
}

bool make_readh_request(int uartn, int slave_id, int address, int len, my_cb_type cb, void *userdata)
{
  //LOG(LL_INFO, ("readh! %d", slave_id));
  clear_pending_transaction(uartn);
  set_pending_transaction(uartn, slave_id, MODBUS_READH_FUNCTION, address, len, cb, userdata);

  if (!send_read_holding_request(uartn, MODBUS_READH_FUNCTION, slave_id, address, len))
  {
    clear_pending_transaction(uartn);
  }
  return true;
}

bool make_readi_request(int uartn, int slave_id, int address, int len, my_cb_type cb, void *userdata)
{
  //LOG(LL_INFO, ("readh! %d", slave_id));
  clear_pending_transaction(uartn);
  set_pending_transaction(uartn, slave_id, MODBUS_READI_FUNCTION, address, len, cb, userdata);
  if (!send_read_holding_request(uartn, MODBUS_READI_FUNCTION, slave_id, address, len))
  {
    clear_pending_transaction(uartn);
  }
  return true;
}

int verify_last_transaction(modbus_state *state)
{
  if (state->last_transaction_func_expected != state->input_buffer[1])
    return MB_ERROR_FUNC_MISMATCH;

  if (state->last_transaction_slave_expected != state->input_buffer[0])
    return MB_ERROR_SLAVE_MISMATCH;

  uint16_t cnt_expt = state->last_transaction_bytes_expected;
  uint16_t crc_c = compute_CRC16(state->input_buffer, cnt_expt - 2);
  if (crc_c != make_word(state->input_buffer[cnt_expt - 1], state->input_buffer[cnt_expt - 2]))
    return MB_ERROR_CRC_MISMATCH;

  if (state->input_buffer[1] == MODBUS_WRITEH_FUNCTION && state->last_transaction_address_expected != make_word(state->input_buffer[2], state->input_buffer[3]))
    return MB_ERROR_ADDRESS_MISMATCH;
  if (state->input_buffer[1] == MODBUS_WRITE_MULTIH_FUNCTION && state->last_transaction_address_expected != make_word(state->input_buffer[2], state->input_buffer[3]))
    return MB_ERROR_ADDRESS_MISMATCH;

  return MB_ERROR_NO_ERROR;
}

void serial_dispatcher(int uartn, void *arg)
{
  uint16_t rx_pending;
  modbus_state *state = (modbus_state *)arg;
  if ((rx_pending = mgos_uart_read_avail(uartn)) > 0)
  {
    uint8_t tempb[rx_pending + 1];
    uint16_t rxed = mgos_uart_read(uartn, tempb, rx_pending + 1);
    if (state->pending_transaction)
    {
      double now = millis();
      state->transaction_last_rx_ts = now;
      uint16_t initial_cnt = state->input_buffer_cnt;
      for (int i = initial_cnt; i < (initial_cnt + rxed); i++)
      {
        state->input_buffer[i] = tempb[i - initial_cnt];
        state->input_buffer_cnt++;
      }
      //LOG(LL_INFO, ("rxed! initial cnt %d, after cnt %d", initial_cnt, state->input_buffer_cnt));
      if (state->input_buffer_cnt >= state->last_transaction_bytes_expected)
      {
        if (state->completion_cb)
        {
          int code = verify_last_transaction(state);
          if (code == MB_ERROR_NO_ERROR)
          {
            uint8_t *p = state->input_buffer;
            //LOG(LL_INFO, ("recibido: %X %X %X %X %X %X %X %X %X ", p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8]));
            state->completion_cb(true, NULL, state->last_transaction_bytes_expected, state->userdata);
          }
          else
          {
            switch (code)
            {
            case MB_ERROR_CRC_MISMATCH:
              state->completion_cb(false, MB_ERROR_CRC_MISMATCH_msg, sizeof(MB_ERROR_CRC_MISMATCH_msg), state->userdata);
              break;
            case MB_ERROR_FUNC_MISMATCH:
              state->completion_cb(false, MB_ERROR_FUNC_MISMATCH_msg, sizeof(MB_ERROR_FUNC_MISMATCH_msg), state->userdata);
              break;
            case MB_ERROR_ADDRESS_MISMATCH:
              state->completion_cb(false, MB_ERROR_ADDRESS_MISMATCH_msg, sizeof(MB_ERROR_ADDRESS_MISMATCH_msg), state->userdata);
              break;
            case MB_ERROR_SLAVE_MISMATCH:
              state->completion_cb(false, MB_ERROR_SLAVE_MISMATCH_msg, sizeof(MB_ERROR_SLAVE_MISMATCH_msg), state->userdata);
              break;
            }
          }
        }
        clear_pending_transaction(uartn);
      }
    }
    else
    {
    }
  }
}

void modbus_ticker(void *arg)
{
  modbus_state *state = (modbus_state *)arg;
  if (state->pending_transaction)
  {
    if (tx_buffer_pending(state->uartn) == 0)
    {
      if ((millis() - state->transaction_last_rx_ts) >= state->transaction_timeout)
      {
        // TODO race condition??
        if (state->completion_cb)
        {
          state->completion_cb(false, "timeout", sizeof("timeout"), state->userdata);
        }
        clear_pending_transaction(state->uartn);
      }
    }
    else
    {
      state->transaction_last_rx_ts = millis();
    }
  }
  // if(millis() - state->alive_ts > 1000.0){
  //   LOG(LL_INFO, ("hello from ticker %d", state->uartn));
  //   state->alive_ts = millis();
  // }
}

void setup_sentinels(int uartn, double resp_timeout)
{
  void *state = (void *)&mb_states[uartn];
  mgos_uart_set_dispatcher(uartn, serial_dispatcher, state);
  mgos_set_timer(20, MGOS_TIMER_REPEAT, modbus_ticker, state);
  mb_states[uartn].transaction_timeout = resp_timeout;
}

// bool process_buffer_as_read_holding_resp()
// {
//   return false;
// }

bool mgos_modbus_master_simplified_ansic_init(void)
{
  for (int i = 0; i < MGOS_MAX_NUM_UARTS; i++)
  {
    mb_states[i].uartn = i;
    clear_pending_transaction(i);
  }
  return true;
}

int get_regval_from_readh_response_with_offset(int uartn, int idx, int offset)
{
  uint8_t *ptr = &mb_states[uartn].input_buffer[3 + (idx * 2) + offset];
  return make_word(*ptr, *(ptr + 1));
}

int get_regval_from_readh_response(int uartn, int idx)
{
  return get_regval_from_readh_response_with_offset(uartn, idx, 0);
}

double get_float_from_readh_response_with_offset(int uartn, int idx, int offset)
{
  uint8_t *ptr = &mb_states[uartn].input_buffer[3 + (idx * 4) + offset];
  return bin_to_float(ptr);
}

double get_float_from_readh_response(int uartn, int idx)
{
  return get_float_from_readh_response_with_offset(uartn, idx, 0);
}

int get_long_from_readh_response_with_offset(int uartn, int idx, int offset)
{
  uint8_t *ptr = &mb_states[uartn].input_buffer[3 + (idx * 4) + offset];
  return bin_to_long(ptr);
}

int get_long_from_readh_response(int uartn, int idx)
{
  return get_long_from_readh_response_with_offset(uartn, idx, 0);
}

double get_float_2_from_readh_response_with_offset(int uartn, int idx, int offset)
{
  uint8_t *ptr = &mb_states[uartn].input_buffer[3 + (idx * 4) + offset];
  return bin_to_float2(ptr);
}

double get_float_2_from_readh_response(int uartn, int idx)
{
  return get_float_2_from_readh_response_with_offset(uartn, idx, 0);
}

int get_long_2_from_readh_response_with_offset(int uartn, int idx, int offset)
{
  uint8_t *ptr = &mb_states[uartn].input_buffer[3 + (idx * 4) + offset];
  return bin_to_long2(ptr);
}

int get_long_2_from_readh_response(int uartn, int idx)
{
  return get_long_2_from_readh_response_with_offset(uartn, idx, 0);
}

char get_byte_from_response(int uartn, int idx)
{
  return mb_states[uartn].input_buffer[3+idx];
}

uint8_t * get_response_buffer(int uartn)
{
  return mb_states[uartn].input_buffer + 3;
} 

typedef union {
  uint8_t bin[4];
  uint16_t bin16[2];
  float fvalue;
  int lvalue;
} modbus_f_converter;

double bin_to_float(uint8_t *data)
{
  modbus_f_converter myconv;
  for (int8_t i = 3; i >= 0; i--)
  {
    myconv.bin[i] = *data;
    data++;
  }
  return myconv.fvalue;
}

int bin_to_long(uint8_t *data)
{
  modbus_f_converter myconv;
  for (int8_t i = 3; i >= 0; i--)
  {
    myconv.bin[i] = *data;
    data++;
  }
  return myconv.lvalue;
}

int bin_to_long2(uint8_t *data)
{
  // modbus_f_converter myconv;
  // myconv.bin16[1] = ;
  // myconv.bin16[0] = ;
  return (make_word(*(data + 2), *(data + 3)) << 16) | make_word(*(data + 0), *(data + 1));
}

double bin_to_float2(uint8_t *data)
{
  modbus_f_converter myconv;
  //myconv.bin16[1] = make_word(*(data+1),*(data+0));
  //myconv.bin16[0] = make_word(*(data+3),*(data+2));
  myconv.lvalue = (make_word(*(data + 2), *(data + 3)) << 16) | make_word(*(data + 0), *(data + 1));
  return myconv.fvalue;
}